package Person;

public class Person {
    private String personName;
    private int personHeight;
    private String personGender;
    private String personHometown;

    public Person(String personName, int personHeight, String personGender, String personHometown){
        this.personName = personName;
        if (personHeight > 0){
            this.personHeight = personHeight;
        }
        this.personGender = personGender;
        this.personHometown = personHometown;
    }
    public Person(){
        personName = "Unknown";
        personGender = "Unknown";
        personHometown = "Unknown";
    }

    public String personGreeting(){
        return personName + ": Greetings adventurer! I'm " + personName + ", welcome to the town of " + personHometown + ". "
                + "I couldn't believe my eyes when I saw you from a far. "
                + "You have a brave looking spirit and it's a brave hero that this town desperately needs!";
    }

    public String personGreeting(Person otherPerson){
        if(otherPerson.personHometown.equals(personHometown)){
            return personName + ": I know you " + otherPerson.personName + "!";
        }
        return personGreeting();
    }

    public float getHeightInches(){
        return Math.round(personHeight * 0.3937 * 100f) / 100f;
    }

    //Getters and Setters
    public String getPersonName(){
        return personName;
    }
    public void setPersonName(String personName){
        this.personName = personName;
    }

    public int getPersonHeight(){
        return personHeight;
    }
    public void setPersonHeight(int personHeight){
        if(personHeight > 0)
            this.personHeight = personHeight;
    }

    public String getPersonGender(){
        return personGender;
    }
    public void setPersonGender(String personGender){
        this.personGender = personGender;
    }

    public String getPersonHometown(){
        return personHometown;
    }
    public void setPersonHometown(String personHometown){
        this.personHometown = personHometown;
    }
}

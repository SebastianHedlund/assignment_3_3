import Person.Person;

public class Main {
    public static void main(String[] args) {
        Person sebastian = new Person("Sebastian", 180, "Male", "Södertälje");
        Person ellen = new Person("Ellen", 165, "Female", "Södertälje");
        Person batman = new Person("Batman", 188, "Justice", "Gotham City");
        Person genericNpc = new Person();

        genericNpc.setPersonName("Marshal Dughan");
        genericNpc.setPersonHometown("Goldshire");

        System.out.println(genericNpc.personGreeting(sebastian));
        System.out.println(sebastian.personGreeting(ellen));

        System.out.println("Batman's height in inches is: " + batman.getHeightInches());
    }
}
